import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Thrower {
    public void throwException() throws IOException {
        throw new IOException();
    }

    public void throwCustomException() throws CustomException {
        throw new CustomException();
    }

    public String readLine() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            return reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }
}
