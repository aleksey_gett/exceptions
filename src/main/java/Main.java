import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        Thrower thrower = new Thrower();

        try {
            thrower.throwException();
        } catch (IOException e) {
            System.out.println("IOException");
        }

        try {
            thrower.throwCustomException();
        } catch (CustomException e) {
            System.out.println(e.exception);
        }

        System.out.println(thrower.readLine());
    }
}
